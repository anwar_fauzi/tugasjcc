import 'package:feda_clover/shared/bottom_navbar.dart';
import 'package:feda_clover/shared/theme.dart';
import 'package:flutter/material.dart';

class RegisterPage extends StatelessWidget {
  /// Class RegisterPage menampilkan layar pendaftaran untuk user baru
  const RegisterPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    // Widget inputFullname digunakan untuk menampilkan textfield untuk mengisi nama lengkap
    Widget inputFullname() {
      return Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text(
            'Nama Lengkap',
            style: blackTextStyle.copyWith(fontSize: 16),
          ),
          const SizedBox(height: 8),
          TextFormField(
            cursorColor: blackColor,
            decoration: InputDecoration(
              hintText: 'Nama Lengkap',
              border: OutlineInputBorder(
                borderRadius: BorderRadius.circular(8),
              ),
              focusedBorder: OutlineInputBorder(
                borderSide: BorderSide(color: green1Color),
                borderRadius: BorderRadius.circular(8),
              ),
            ),
          )
        ],
      );
    }

    // Widget inputUsername digunakan untuk menampilkan textfield untuk mengisi username
    Widget inputUsername() {
      return Container(
        margin: const EdgeInsets.only(top: 16),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Text(
              'Username',
              style: blackTextStyle.copyWith(fontSize: 16),
            ),
            const SizedBox(height: 8),
            TextFormField(
              cursorColor: blackColor,
              decoration: InputDecoration(
                hintText: 'Username',
                border: OutlineInputBorder(
                  borderRadius: BorderRadius.circular(8),
                ),
                focusedBorder: OutlineInputBorder(
                  borderSide: BorderSide(color: green1Color),
                  borderRadius: BorderRadius.circular(8),
                ),
              ),
            )
          ],
        ),
      );
    }

    // Widget inputEmail digunakan untuk menampilkan textfield untuk mengisi email
    Widget inputEmail() {
      return Container(
        margin: const EdgeInsets.only(top: 16),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Text(
              'Email',
              style: blackTextStyle.copyWith(fontSize: 16),
            ),
            const SizedBox(height: 8),
            TextFormField(
              cursorColor: blackColor,
              decoration: InputDecoration(
                hintText: 'your@email.com',
                border: OutlineInputBorder(
                  borderRadius: BorderRadius.circular(8),
                ),
                focusedBorder: OutlineInputBorder(
                  borderSide: BorderSide(color: green1Color),
                  borderRadius: BorderRadius.circular(8),
                ),
              ),
            )
          ],
        ),
      );
    }

    // Widget inputPhone digunakan untuk menampilkan textfield untuk mengisi nomor telepon
    Widget inputPhone() {
      return Container(
        margin: const EdgeInsets.only(top: 16),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Text(
              'No. Telepon',
              style: blackTextStyle.copyWith(fontSize: 16),
            ),
            const SizedBox(height: 8),
            TextFormField(
              cursorColor: blackColor,
              decoration: InputDecoration(
                hintText: '+62 xxxxxxxxx',
                border: OutlineInputBorder(
                  borderRadius: BorderRadius.circular(8),
                ),
                focusedBorder: OutlineInputBorder(
                  borderSide: BorderSide(color: green1Color),
                  borderRadius: BorderRadius.circular(8),
                ),
              ),
            )
          ],
        ),
      );
    }

    // Widget inputPassword digunakan untuk menampilkan textfield untuk mengisi password
    Widget inputPassword() {
      return Container(
        margin: const EdgeInsets.only(top: 16),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Text(
              'Password',
              style: blackTextStyle.copyWith(fontSize: 16),
            ),
            const SizedBox(height: 8),
            TextFormField(
              cursorColor: blackColor,
              decoration: InputDecoration(
                hintText: 'Password',
                border: OutlineInputBorder(
                  borderRadius: BorderRadius.circular(8),
                ),
                focusedBorder: OutlineInputBorder(
                  borderSide: BorderSide(color: green1Color),
                  borderRadius: BorderRadius.circular(8),
                ),
              ),
            ),
          ],
        ),
      );
    }

    // Widget inputConfirmPasswrod digunakan untuk menampilkan textfield untuk mengisi konfirmasi ulang password
    Widget inputConfirmPassword() {
      return Container(
        margin: const EdgeInsets.only(top: 16),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Text(
              'Konfirmasi Password',
              style: blackTextStyle.copyWith(fontSize: 16),
            ),
            const SizedBox(height: 8),
            TextFormField(
              cursorColor: blackColor,
              decoration: InputDecoration(
                hintText: 'Konfirmasi Password Anda',
                border: OutlineInputBorder(
                  borderRadius: BorderRadius.circular(8),
                ),
                focusedBorder: OutlineInputBorder(
                  borderSide: BorderSide(color: green1Color),
                  borderRadius: BorderRadius.circular(8),
                ),
              ),
            ),
          ],
        ),
      );
    }

    // Widget title digunakan untuk menampilkan judul dari layar register
    Widget title() {
      return Container(
        margin: const EdgeInsets.only(top: 66),
        child: Column(
          children: [
            Text(
              'Daftar Akun',
              style: blackTextStyle.copyWith(fontSize: 32, fontWeight: bold),
            ),
            const SizedBox(height: 16),
            Text.rich(
                TextSpan(
                    text: 'Silahkan daftar akun',
                    style: grey3TextStyle.copyWith(fontSize: 16),
                    children: [
                      TextSpan(
                        text: ' Klover ',
                        style: green1TextStyle.copyWith(
                            fontSize: 16, fontWeight: bold),
                      ),
                      TextSpan(
                          text:
                              'Anda dengan mengisi informasi yang dibutuhkan!',
                          style: grey3TextStyle.copyWith(fontSize: 16)),
                    ]),
                textAlign: TextAlign.center),
          ],
        ),
      );
    }
    // ** Widget textFormField digunakan untuk menampilkan keseluruhan dari FormField yang akan ditampilkan
    // mulai dari inputFullname, inputUsername, inputEmail, inputPhone, inputPassword, dan inputConfirmPassword.
    // */

    Widget textFormField() {
      return Container(
        margin: const EdgeInsets.only(top: 32),
        child: Column(
          children: [
            inputFullname(), //menampilkan textfield fullname
            inputUsername(), //menampilkan textfield username
            inputEmail(), //menampilkan textfield email
            inputPhone(), //menampilkan textfield no. telepon
            inputPassword(), //menampilkan textfield password
            inputConfirmPassword(), //menampilkan textfield konfirmasi password
          ],
        ),
      );
    }

    // Widget buttonProcess menampilkan tombol daftar, dan juga tombol untuk ke halaman login
    Widget buttonProcess() {
      return Container(
        margin: const EdgeInsets.only(top: 32, bottom: 44),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            TextButton(
              onPressed: () => Navigator.pushNamed(context, '/login'),
              child: Text.rich(
                TextSpan(
                    text: 'Sudah Punya Akun ? ',
                    style: blackTextStyle.copyWith(fontSize: 16),
                    children: [
                      TextSpan(
                          text: 'Login',
                          style: green1TextStyle.copyWith(
                              fontSize: 16, fontWeight: bold))
                    ]),
              ),
            ),
            Container(
              width: 77,
              height: 42,
              margin: const EdgeInsets.only(left: 50),
              child: TextButton(
                onPressed: () {
                  Navigator.pushNamed(context, '/main');
                },
                style: TextButton.styleFrom(
                  backgroundColor: green1Color,
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(8),
                  ),
                ),
                child: Text(
                  'Daftar',
                  style: white1TextStyle.copyWith(fontSize: 16),
                ),
              ),
            ),
          ],
        ),
      );
    }

    return Scaffold(
        body: SafeArea(
      child: Container(
        padding: EdgeInsets.symmetric(horizontal: defaultMargin),
        child: ListView(
          children: [
            title(),
            textFormField(),
            buttonProcess(),
            const BottomNavbar(),
          ],
        ),
      ),
    ));
  }
}
