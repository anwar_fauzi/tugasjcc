import 'dart:async';
import 'package:feda_clover/shared/theme.dart';
import 'package:flutter/material.dart';

class SplashScreenPage extends StatefulWidget {

/// Class SplashScreenPage menampilkan layar awal ketika aplikasi dibuka,
/// 
/// durasi penampilan SplashScreen diatur selama 3 detik
  const SplashScreenPage({Key? key}) : super(key: key);

  @override
  _SplashScreenPageState createState() => _SplashScreenPageState();
}

class _SplashScreenPageState extends State<SplashScreenPage> {
  @override
  // ** Durasi layar SplashScreen ditampilkan, 
  // di atur dalam satuan seconds, selama 3 seconds */
  void initState() {
    Timer(const Duration(seconds: 3), () {
      Navigator.pushNamed(context, '/main');
    });
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Stack(
        children: [
          Container(
            decoration: const BoxDecoration(
              image: DecorationImage(
                fit: BoxFit.cover,
                image: AssetImage('assets/background.png'),
              ),
            ),
          ),
          Center(
            child: Container(
              width: 600,
              height: 290,
              margin: const EdgeInsets.symmetric(horizontal: 20),
              decoration: BoxDecoration(
                color: white1Color,
                borderRadius: BorderRadius.circular(40),
              ),
              child: Center(
                child: Container(
                  width: 500,
                  height: 189,
                  margin: const EdgeInsets.all(50),
                  decoration: const BoxDecoration(
                    image: DecorationImage(
                      image: AssetImage('assets/logo.png'),
                    ),
                  ),
                ),
              ),
            ),
          )
        ],
      ),
    );
  }
}
