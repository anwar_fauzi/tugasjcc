import 'ikan_hiu.dart';
import 'ikan_layur.dart';
import 'ikan_pari.dart';
import 'ikan_tongkol.dart';

void main() {
  IkanTongkol it = new IkanTongkol();
  IkanLayur il = new IkanLayur();
  IkanPari ip = new IkanPari();
  IkanHiu ih = new IkanHiu();

  it.tenagaIkan = 1;
  il.tenagaIkan = 7;
  ip.tenagaIkan = 5;
  ih.tenagaIkan = 13;

  print('Tenaga Ikan Tongkol : ${it.tenagaIkan}');
  print('Tenaga Ikan Layur : ${il.tenagaIkan}');
  print('Tenaga Ikan Pari : ${ip.tenagaIkan}');
  print('Tenaga Ikan Hiu : ${ih.tenagaIkan}');
  print('');
  print('object Ikan Tongkol : ${it.bergerombol()}');
  print('object Ikan Layur : ${il.sendiri()}');
  print('object Ikan Pari : ${ip.hap()}');
  print('object Ikan Hiu : ${ih.wush()}');
}

