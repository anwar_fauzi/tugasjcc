class Segitiga{
   double? _alas;
   double? _tinggi;

  void set alas(double value){
    if (value < 0){
      value *= -1;
    }
    _alas = value;
  }
  double get alas{
    return _alas!;
  }
  void set tinggi(double value){
    if (value < 0){
      value *= -1;
    }
    _tinggi = value;
  }
  double get tinggi{
    return _tinggi!;
  }
  double get luas => this._alas! * this._tinggi! / 2;
}
