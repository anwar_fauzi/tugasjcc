import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';


part './bottom_navbar.dart';

part '../ui/pages/splash_screen_page.dart';
part '../ui/pages/login_page.dart';
part '../ui/pages/home/home_page.dart';
part '../ui/pages/home/profile_page.dart';
part '../ui/pages/home/detail_activity_page.dart';

part '../widget/button_splash_screen.dart';
part '../widget/icon_bar_login_page.dart';
part '../widget/card_browse_job_title.dart';
part '../widget/card_popular_job.dart';
part '../widget/custom_text_button.dart';

Color primaryColor = const Color(0xffFF725E);
Color whiteColor = const Color(0xffFFFFFF);
Color grey1Color = const Color(0xff7E7D7D);
Color grey3Color = const Color(0xff9C9C9C);
Color grey4Color = const Color(0xffAEAEAE);
Color blackColor = const Color(0xff000000);
Color subtitleColor = const Color(0xffC1C1C1);
Color containerColor = const Color(0xffF6F6F6);

Color backgroundColor = const Color(0xffFCFCFC);

TextStyle primaryTextStyle = GoogleFonts.redHatText(color: primaryColor);
TextStyle whiteTextStyle = GoogleFonts.redHatText(color: whiteColor);
TextStyle grey1TextStyle = GoogleFonts.redHatText(color: grey1Color);
TextStyle subtitleTextStyle = GoogleFonts.redHatText(color: subtitleColor);
TextStyle grey3TextStyle = GoogleFonts.redHatText(color: grey3Color);
TextStyle grey4TextStyle = GoogleFonts.redHatText(color: grey4Color);
TextStyle blackTextStyle = GoogleFonts.redHatText(color: blackColor);

FontWeight light = FontWeight.w300;
FontWeight medium = FontWeight.w500;
FontWeight semiBold = FontWeight.w600;
FontWeight bold = FontWeight.w700;
FontWeight extraBold = FontWeight.w800;
FontWeight black = FontWeight.w900;

Shadow shadow = const Shadow(
  offset: Offset(0.0, 2.5),
  blurRadius: 6.0,
  color: Color.fromARGB(140, 0, 0, 0),
);

BoxShadow boxShadow = const BoxShadow(
  offset: Offset(0.0, 3.0),
  blurRadius: 2.5,
  color: Color.fromARGB(100, 0, 0, 0),
);
