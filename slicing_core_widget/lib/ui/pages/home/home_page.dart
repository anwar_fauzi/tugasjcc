part of '../../../shared/theme.dart';

class HomePage extends StatelessWidget {
  const HomePage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      bottomNavigationBar: const BottomNavbar(
        homeIsSelected: true,
      ),
      body: SingleChildScrollView(
        child: SafeArea(
          child: Container(
            margin: const EdgeInsets.symmetric(horizontal: 38),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Container(
                      width: 14,
                      height: 14,
                      margin: const EdgeInsets.only(top: 51),
                      decoration: const BoxDecoration(
                        image: DecorationImage(
                          image: AssetImage('assets/icon_grid.png'),
                        ),
                      ),
                    ),
                    Container(
                      width: 15,
                      height: 15,
                      margin: const EdgeInsets.only(top: 51),
                      decoration: const BoxDecoration(
                        image: DecorationImage(
                          image: AssetImage('assets/icon_bell.png'),
                        ),
                      ),
                    ),
                  ],
                ),
                const SizedBox(height: 39),
                Text('Welcome', style: grey3TextStyle),
                const SizedBox(height: 7),
                Text(
                  'Find your dream job!',
                  style:
                      blackTextStyle.copyWith(fontSize: 18, fontWeight: bold),
                ),
                const SizedBox(height: 27),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Container(
                      width: 240,
                      height: 35,
                      margin: const EdgeInsets.only(right: 7),
                      decoration: BoxDecoration(
                        color: containerColor,
                        borderRadius: BorderRadius.circular(8),
                      ),
                      child: Row(
                        children: [
                          Container(
                            width: 18,
                            height: 18,
                            margin: const EdgeInsets.symmetric(horizontal: 14),
                            decoration: const BoxDecoration(
                              image: DecorationImage(
                                image: AssetImage('assets/icon_search.png'),
                              ),
                            ),
                          ),
                          Expanded(
                            child: SizedBox(
                              height: 35,
                              child: TextFormField(
                                cursorColor: blackColor,
                                style: blackTextStyle,
                                decoration: InputDecoration(
                                    contentPadding: EdgeInsets.zero,
                                    border: InputBorder.none,
                                    hintText: 'Search your best job!',
                                    hintStyle: grey3TextStyle),
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),
                    Container(
                      width: 40,
                      height: 35,
                      decoration: BoxDecoration(
                        color: primaryColor,
                        borderRadius: BorderRadius.circular(8),
                      ),
                      child: Center(
                        child: Container(
                          width: 20,
                          height: 16,
                          decoration: const BoxDecoration(
                              image: DecorationImage(
                                  image:
                                      AssetImage('assets/icon_coolicon.png'))),
                        ),
                      ),
                    ),
                  ],
                ),
                const SizedBox(height: 7),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Text(
                      'Browse By Job Title',
                      style: blackTextStyle.copyWith(
                          fontSize: 12, fontWeight: bold),
                    ),
                    TextButton(
                      onPressed: () {},
                      child: Text(
                        'See More',
                        style: primaryTextStyle.copyWith(
                            fontSize: 12, fontWeight: bold),
                      ),
                    ),
                  ],
                ),
                const SizedBox(
                  height: 12,
                  width: 200,
                ),
                SingleChildScrollView(
                  scrollDirection: Axis.horizontal,
                  child: Row(
                    children: const [
                      CardBrowseJob(title: 'UI/UX Design'),
                      CardBrowseJob(title: 'Web Dev'),
                      CardBrowseJob(title: 'Design Interior'),
                      CardBrowseJob(title: 'Production'),
                      CardBrowseJob(title: 'QC'),
                    ],
                  ),
                ),
                const SizedBox(height: 20),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Text(
                      'Popular Job',
                      style: blackTextStyle.copyWith(
                          fontSize: 12, fontWeight: bold),
                    ),
                    TextButton(
                      onPressed: () {},
                      child: Text(
                        'See more',
                        style: primaryTextStyle.copyWith(
                            fontSize: 12, fontWeight: bold),
                      ),
                    ),
                  ],
                ),
                const SizedBox(height: 18),
                SingleChildScrollView(
                  scrollDirection: Axis.horizontal,
                  child: Row(
                    children: const [
                      CardPopularJob(
                        imageUrl: 'assets/image_facebook.png',
                        name: 'Facebook',
                        job: 'Facebook',
                        location: 'Jakarta, Indonesia',
                        route: '/detail',
                      ),
                      SizedBox(width: 21),
                      CardPopularJob(
                          imageUrl: 'assets/image_fahendra.png',
                          name: 'Fahendra Group',
                          job: 'UI/UX Design',
                          location: 'Bojonegoro, Indonesia'),
                    ],
                  ),
                ),
                const SizedBox(height: 19),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Text(
                      'Recent Job',
                      style: blackTextStyle.copyWith(
                          fontSize: 12, fontWeight: bold),
                    ),
                    TextButton(
                      onPressed: () {},
                      child: Text(
                        'See more',
                        style: primaryTextStyle.copyWith(
                            fontSize: 12, fontWeight: bold),
                      ),
                    ),
                  ],
                ),
                const SizedBox(height: 16),
                Opacity(
                  opacity: 0.5,
                  child: Container(
                    width: double.infinity,
                    height: 110,
                    decoration: BoxDecoration(
                      color: primaryColor,
                      borderRadius: BorderRadius.circular(10),
                    ),
                    child: Row(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Container(
                          width: 35,
                          height: 35,
                          margin: const EdgeInsets.only(
                              left: 11, right: 17, top: 11),
                          decoration: BoxDecoration(
                            color: whiteColor,
                            borderRadius: BorderRadius.circular(5),
                          ),
                          child: Container(
                            width: 27,
                            height: 27,
                            decoration: const BoxDecoration(
                              image: DecorationImage(
                                image: AssetImage('assets/image_figma.png'),
                              ),
                            ),
                          ),
                        ),
                        Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            const SizedBox(height: 11),
                            Text(
                              'Figma Design',
                              style: blackTextStyle.copyWith(
                                  fontSize: 15, fontWeight: bold),
                            ),
                            const SizedBox(height: 7),
                            Text(
                              'Figma Product Designer',
                              style: grey1TextStyle.copyWith(
                                  fontSize: 13, fontWeight: bold),
                            ),
                          ],
                        ),
                        const Spacer(),
                        Container(
                          width: 12,
                          height: 12,
                          margin: const EdgeInsets.only(right: 17, top: 15),
                          child: Icon(
                            Icons.favorite_border,
                            color: grey1Color,
                          ),
                        )
                      ],
                    ),
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
