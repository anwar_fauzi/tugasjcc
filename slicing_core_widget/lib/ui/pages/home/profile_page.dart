part of '../../../shared/theme.dart';

class ProfilePage extends StatelessWidget {
  const ProfilePage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      bottomNavigationBar: const BottomNavbar(
        profileIsSelected: true,
      ),
      body: SafeArea(
        child: SingleChildScrollView(
          child: Column(
            children: [
              Align(
                alignment: Alignment.centerLeft,
                child: Container(
                  width: 30,
                  height: 30,
                  margin: const EdgeInsets.only(left: 34, top: 48),
                  decoration: BoxDecoration(
                      color: primaryColor, shape: BoxShape.circle),
                  child: SizedBox(
                    width: 3,
                    height: 5,
                    child: Icon(
                      Icons.arrow_back_ios_new,
                      color: whiteColor,
                    ),
                  ),
                ),
              ),
              const SizedBox(height: 19),
              SizedBox(
                width: 87,
                height: 90,
                child: Stack(
                  children: [
                    Container(
                      width: 77,
                      height: 80,
                      decoration: const BoxDecoration(
                        image: DecorationImage(
                          image: AssetImage('assets/image_kevin.png'),
                        ),
                      ),
                    ),
                    Align(
                      alignment: Alignment.bottomRight,
                      child: Container(
                          width: 30,
                          height: 30,
                          decoration: BoxDecoration(
                            color: primaryColor,
                            borderRadius: BorderRadius.circular(10),
                          ),
                          child: Icon(Icons.add, color: whiteColor)),
                    ),
                  ],
                ),
              ),
              const SizedBox(height: 10),
              Text(
                'Muhammad Kelvin M.F',
                style: blackTextStyle.copyWith(fontSize: 18, fontWeight: bold),
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Text(
                    'Type to edit',
                    style: grey1TextStyle.copyWith(fontSize: 11),
                  ),
                   Icon(Icons.edit, size: 14),
                  
                ],
              ),
              const SizedBox(height: 40),
              Container(
                width: 235,
                height: 280,
                decoration: BoxDecoration(
                    color: whiteColor,
                    borderRadius: BorderRadius.circular(15),
                    border: Border.all(color: grey1Color)),
                child: Column(
                  children: [
                    Row(
                      children: [
                        Container(
                          width: 22,
                          height: 22,
                          margin: const EdgeInsets.only(
                              top: 17, left: 12, right: 7),
                          decoration: const BoxDecoration(
                            image: DecorationImage(
                              image:
                                  AssetImage('assets/icon_business_person.png'),
                            ),
                          ),
                        ),
                        Column(
                          children: [
                            const SizedBox(height: 8),
                            Text(
                              'Apply Your Job',
                              style: blackTextStyle.copyWith(
                                  fontSize: 15, fontWeight: bold),
                            ),
                            Text(
                              '20 apply',
                              style: grey1TextStyle.copyWith(
                                  fontSize: 15, fontWeight: bold),
                            ),
                          ],
                        ),
                        const Spacer(),
                        Icon(Icons.arrow_forward_ios, color: blackColor),
                        const SizedBox(width: 10),
                      ],
                    ),
                    const Divider(thickness: 2),
                    Row(
                      children: [
                        Container(
                          width: 22,
                          height: 22,
                          margin: const EdgeInsets.only(
                              top: 17, left: 12, right: 7),
                          child: Icon(Icons.business_center_sharp,
                              color: grey1Color),
                        ),
                        Column(
                          children: [
                            const SizedBox(height: 8),
                            Text(
                              'Ready To Apply',
                              style: blackTextStyle.copyWith(
                                  fontSize: 15, fontWeight: bold),
                            ),
                            Text(
                              '280 Job Vacancy',
                              style: grey1TextStyle.copyWith(
                                  fontSize: 15, fontWeight: bold),
                            ),
                          ],
                        ),
                        const Spacer(),
                        Icon(Icons.arrow_forward_ios, color: blackColor),
                        const SizedBox(width: 10),
                      ],
                    ),
                    const Divider(thickness: 2),
                    Row(
                      children: [
                        Container(
                          width: 22,
                          height: 22,
                          margin: const EdgeInsets.only(
                              top: 17, left: 12, right: 7),
                          decoration: const BoxDecoration(
                            image: DecorationImage(
                              image: AssetImage('assets/icon_social_media.png'),
                            ),
                          ),
                        ),
                        Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            const SizedBox(height: 8),
                            Text(
                              'Sosial Media',
                              style: blackTextStyle.copyWith(
                                  fontSize: 15, fontWeight: bold),
                            ),
                            Row(
                              children: [
                                Container(
                                  width: 14,
                                  height: 14,
                                  decoration: const BoxDecoration(
                                      image: DecorationImage(
                                          image: AssetImage(
                                              'assets/image_instagram.png'))),
                                ),
                                const SizedBox(width: 3),
                                Text(
                                  'instagram.com/kelvin_mkmf/',
                                  style: grey1TextStyle.copyWith(
                                      fontSize: 10, fontWeight: bold),
                                ),
                              ],
                            ),
                            const SizedBox(height: 6),
                            Row(
                              children: [
                                Container(
                                  width: 14,
                                  height: 14,
                                  decoration: const BoxDecoration(
                                      image: DecorationImage(
                                          image: AssetImage(
                                              'assets/image_linkedin.png'))),
                                ),
                                const SizedBox(width: 3),
                                SizedBox(
                                  width: 141,
                                  child: Text(
                                    'linkedin.com/in/m-kelvin-madrianto-fahendra-8361bb16a',
                                    style: grey1TextStyle.copyWith(
                                        fontSize: 10, fontWeight: bold),
                                  ),
                                ),
                              ],
                            ),
                          ],
                        ),
                        Icon(Icons.arrow_forward_ios, color: blackColor),
                        const SizedBox(width: 10),
                      ],
                    ),
                    const Divider(thickness: 2),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceAround,
                      children: [
                        Container(
                          width: 28,
                          height: 21,
                          decoration: const BoxDecoration(
                            image: DecorationImage(
                              image: AssetImage(
                                'assets/image_kotlin.png',
                              ),
                            ),
                          ),
                        ),
                        Container(
                          width: 35,
                          height: 35,
                          decoration: const BoxDecoration(
                            image: DecorationImage(
                              image: AssetImage(
                                'assets/image_java.png',
                              ),
                            ),
                          ),
                        ),
                        Container(
                          width: 40,
                          height: 27,
                          decoration: const BoxDecoration(
                            image: DecorationImage(
                              image: AssetImage(
                                'assets/image_adobe.png',
                              ),
                            ),
                          ),
                        ),
                        Container(
                          width: 35,
                          height: 40,
                          decoration: const BoxDecoration(
                            image: DecorationImage(
                              image: AssetImage(
                                'assets/image_android_studio.png',
                              ),
                            ),
                          ),
                        ),
                      ],
                    ),
                  ],
                ),
              ),
              const SizedBox(height: 24),
              const SizedBox(
                  width: 200, child: CustomTextButton(title: 'LOGOUT')),
            ],
          ),
        ),
      ),
    );
  }
}
