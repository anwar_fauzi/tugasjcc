part of '../../shared/theme.dart';

class SplashScreenPage extends StatelessWidget {
  const SplashScreenPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: whiteColor,
      body: SingleChildScrollView(
        child: Column(
          children: [
            Container(
              margin: const EdgeInsets.only(left: 34, top: 76),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(
                    'Find Your\nDream Job!',
                    style:
                        primaryTextStyle.copyWith(fontSize: 30, fontWeight: bold, shadows:[shadow],),
                  ),
                  const SizedBox(height: 12),
                  Row(
                    children: [
                      Container(
                        width: 45,
                        height: 5,
                        decoration: BoxDecoration(
                          boxShadow: [boxShadow],
                            color: primaryColor,
                            borderRadius: BorderRadius.circular(5)),
                      ),
                      Container(
                        width: 19,
                        height: 5,
                        margin: const EdgeInsets.only(left: 7),
                        decoration: BoxDecoration(
                          boxShadow: [boxShadow],
                          color: primaryColor,
                          borderRadius: BorderRadius.circular(5),
                        ),
                      ),
                      Container(
                        width: 19,
                        height: 5,
                        margin: const EdgeInsets.only(left: 7),
                        decoration: BoxDecoration(
                          boxShadow: [boxShadow],
                          color: primaryColor,
                          borderRadius: BorderRadius.circular(5),
                        ),
                      ),
                    ],
                  ),
                ],
              ),
            ),
            Container(
              width: 352,
              height: 299,
              decoration: const BoxDecoration(
                image: DecorationImage(
                  image: AssetImage(
                    'assets/image_splash.png',
                  ),
                ),
              ),
            ),
            const ButtonSplashScreen(title: 'POST A JOB', topMargin: 10),
            const ButtonSplashScreen(title: 'FIND A JOB', topMargin: 22, route: '/login',),
            const SizedBox(height: 50),
          ],
        ),
      ),
    );
  }
}
