part of '../../shared/theme.dart';

class LoginPage extends StatelessWidget {
  final bool passwordVisibility = false;
  const LoginPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: backgroundColor,
      body: SafeArea(
        child: SingleChildScrollView(
          child: Column(
            children: [
              Row(
                mainAxisAlignment: MainAxisAlignment.end,
                children: const [
                  IconBar(imageUrl: 'assets/icon_wifi.png', square: 16, top: 7),
                  IconBar(
                      imageUrl: 'assets/icon_signal.png',
                      square: 16,
                      right: 4,
                      top: 7),
                  IconBar(
                      imageUrl: 'assets/icon_batery.png',
                      square: 19,
                      right: 13,
                      top: 7),
                ],
              ),
              Container(
                width: 241,
                height: 241,
                decoration: const BoxDecoration(
                  image: DecorationImage(
                    image: AssetImage('assets/image_login.png'),
                  ),
                ),
              ),
              Container(
                width: double.infinity,
                height: 395,
                padding: const EdgeInsets.symmetric(horizontal: 20),
                decoration: BoxDecoration(
                  color: whiteColor,
                  borderRadius: const BorderRadius.only(
                    topLeft: Radius.circular(20),
                    topRight: Radius.circular(20),
                  ),
                ),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    const SizedBox(height: 20),
                    Text(
                      'Login Now!',
                      style: primaryTextStyle.copyWith(
                          fontSize: 20, fontWeight: bold, shadows: [shadow]),
                    ),
                    const SizedBox(height: 7),
                    Text(
                      'Please enter the details below to continue!',
                      style: subtitleTextStyle.copyWith(fontSize: 11),
                    ),
                    Container(
                      width: double.infinity,
                      height: 35,
                      margin: const EdgeInsets.only(left: 10, right: 10, top: 30),
                      decoration: BoxDecoration(
                          color: containerColor,
                          borderRadius: BorderRadius.circular(7)),
                      child: TextFormField(
                        cursorColor: blackColor,
                        style: blackTextStyle.copyWith(fontSize: 13),
                        decoration: InputDecoration(
                          contentPadding: const EdgeInsets.only(left: 24),
                          hintText: 'Your Email',
                          hintStyle: blackTextStyle.copyWith(fontSize: 13),
                          border: OutlineInputBorder(
                            borderSide: BorderSide(color: grey1Color),
                          ),
                        ),
                      ),
                    ),
                    Container(
                      width: double.infinity,
                      height: 35,
                      margin: const EdgeInsets.only(left: 10, right: 10, top: 30),
                      decoration: BoxDecoration(
                          color: containerColor,
                          borderRadius: BorderRadius.circular(7)),
                      child: TextFormField(
                        obscureText: true,
                        cursorColor: blackColor,
                        style: blackTextStyle.copyWith(fontSize: 13),
                        decoration: InputDecoration(
                          contentPadding: const EdgeInsets.only(left: 24),
                          hintText: 'Your Password',
                          hintStyle: blackTextStyle.copyWith(fontSize: 13),
                          suffixIcon:
                              Icon(Icons.visibility_off, color: blackColor),
                          border: OutlineInputBorder(
                            borderSide: BorderSide(color: grey1Color),
                          ),
                        ),
                      ),
                    ),
                    const SizedBox(height: 8),
                    Container(
                      width: double.infinity,
                      margin: const EdgeInsets.symmetric(horizontal: 10),
                      child: Text(
                        'Forgot Password ?',
                        style: primaryTextStyle.copyWith(
                          fontSize: 12,
                          shadows: [shadow],
                        ),
                        textAlign: TextAlign.end,
                      ),
                    ),
                    const CustomTextButton(title: 'LOGIN', route: '/home'),
                    Container(
                      width: double.infinity,
                      margin: const EdgeInsets.symmetric(vertical: 27),
                      child: InkWell(
                        onTap: (){},
                        child: Text.rich(
                          TextSpan(
                            text: 'Dont have an account! ',
                            style: blackTextStyle.copyWith(fontSize: 12),
                            children: [
                              TextSpan(
                                text: 'Register',
                                style: primaryTextStyle.copyWith(fontSize: 12),
                              ),
                            ],
                          ),
                          textAlign: TextAlign.center,
                        ),
                      ),
                    ),
                  ],
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
