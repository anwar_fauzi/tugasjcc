part of '../shared/theme.dart';

class CardBrowseJob extends StatelessWidget {
  final String title;
  const CardBrowseJob({Key? key,required this.title}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: const EdgeInsets.symmetric(horizontal: 5),
      height: 23,
      margin: const EdgeInsets.only(right: 11),
      decoration: BoxDecoration(
        color: whiteColor,
        borderRadius: BorderRadius.circular(4),
        border: Border.all(color: grey4Color),
        boxShadow: [boxShadow],
      ),
      child: Center(
        child: Text(
          title,
          style: subtitleTextStyle.copyWith(fontSize: 9),
        ),
      ),
    );
  }
}
