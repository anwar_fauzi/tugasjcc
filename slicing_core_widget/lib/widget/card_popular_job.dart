part of '../shared/theme.dart';

class CardPopularJob extends StatelessWidget {
  final String imageUrl;
  final String name;
  final String job;
  final String location;
  final String route;
  const CardPopularJob({Key? key, required this.imageUrl, required this.name, required this.job, required this.location, this.route = '/'}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: ()=>Navigator.pushNamed(context, '/detail'),
      child: Container(
        width: 185,
        height: 127,
        padding: const EdgeInsets.fromLTRB(14, 9, 12, 17),
        decoration: BoxDecoration(
          color: whiteColor,
          borderRadius: BorderRadius.circular(5),
        ),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Container(
                  width: 39,
                  height: 39,
                  margin: const EdgeInsets.only(right: 11),
                  decoration: BoxDecoration(
                    image: DecorationImage(
                      image: AssetImage(imageUrl),
                    ),
                  ),
                ),
                Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(
                      name,
                      style: blackTextStyle.copyWith(fontSize: 13),
                    ),
                    const SizedBox(height: 3),
                    Text(
                      'Owner',
                      style: subtitleTextStyle.copyWith(fontSize: 9),
                    ),
                  ],
                ),
                const SizedBox(
                  width: 12.5,
                  height: 11.26,
                  child: Icon(Icons.favorite_border),
                ),
              ],
            ),
            const SizedBox(height: 14),
            Text(
              job,
              style: blackTextStyle.copyWith(fontSize: 11),
            ),
            const SizedBox(height: 7),
            Row(
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                Container(
                  width: 10,
                  height: 13,
                  margin: const EdgeInsets.only(right: 10),
                  child: Icon(Icons.location_on, color: grey3Color),
                ),
                Text(
                  location,
                  style: subtitleTextStyle.copyWith(fontSize: 9),
                ),
              ],
            ),
          ],
        ),
      ),
    );
  }
}
