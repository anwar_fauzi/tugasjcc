part of '../shared/theme.dart';

class CustomTextButton extends StatelessWidget {
  final String title;
  final String route;
  const CustomTextButton({Key? key, required this.title, this.route = '/'}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      width: double.infinity,
      height: 40,
      margin: const EdgeInsets.symmetric(horizontal: 10, vertical: 19),
      decoration: BoxDecoration(
        color: primaryColor,
        borderRadius: BorderRadius.circular(50),
      ),
      child: TextButton(
          onPressed: () => Navigator.pushNamed(context, route),
          child: Text(
            title,
            style: whiteTextStyle.copyWith(fontWeight: extraBold),
          )),
    );
  }
}
