part of 'function.dart';

/// Function kalikanMath merupakan Function Anonymous
int kalikanMath(int aa, int bb, Function ops) {
  return ops(aa, bb);
}
