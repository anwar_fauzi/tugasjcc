part of 'function.dart';

/// Function lambdaKalikanMath adalah Function yang menggunakan lambda Expression
///
/// bentuk lambda Expression ditandai dengan tanda (sama dengan ditambah arrow), yaitu =>
double lambdaKalikanMath(aa, bb) => aa * bb;
